@extends('AdminPanel.Layouts.GuestMaster')
@section('content')
    <section>
        <div class="container-alt">
            <div class="row">
                <div class="col-sm-12">

                    <div class="wrapper-page">

                        <div class="m-t-40 account-pages">
                            <div class="text-center account-logo-box">
                                {{--<h2 class="text-uppercase">
                                    <a href="index.html" class="text-success">
                                        <span><img src="assets/images/logo.png" alt="" height="36"></span>
                                    </a>
                                </h2>--}}
                                <h4 class="text-uppercase font-bold m-b-0 text-white">{{__('Admin.login')}}</h4>
                            </div>
                            <div class="account-content">
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group ">
                                        <div class="col-xs-12">
                                            <input type="email"
                                                   class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" value="{{ old('email') }}" required autofocus
                                                   placeholder="{{__('Admin.email')}}">
                                            @if ($errors->has('email'))
                                                <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input type="password"
                                                   class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" required placeholder="{{__('Admin.password')}}">
                                            @if ($errors->has('password'))
                                                <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="col-xs-12">
                                            <div class="checkbox checkbox-success">
                                                <input id="checkbox-signup" type="checkbox"
                                                       name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label for="checkbox-signup">
                                                    {{__('Admin.remember')}}
                                                </label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group text-center m-t-30">
                                        <div class="col-sm-12">
                                            <a href="{{ route('password.request') }}" class="text-muted"><i
                                                        class="fa fa-lock m-r-5"></i> {{__('Admin.forgetPassword')}}</a>
                                        </div>
                                    </div>

                                    <div class="form-group account-btn text-center m-t-10">
                                        <div class="col-xs-12">
                                            <button class="btn w-md btn-bordered btn-danger waves-effect waves-light"
                                                    type="submit">{{__('Admin.login')}}
                                            </button>
                                        </div>
                                    </div>

                                </form>

                                <div class="clearfix"></div>

                            </div>
                        </div>
                        <!-- end card-box-->
                    </div>
                    <!-- end wrapper -->

                </div>
            </div>
        </div>
    </section>
@endsection