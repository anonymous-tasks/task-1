@extends('AdminPanel.Layouts.AuthMaster')

@section('style')
    <link href="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/jquery.dataTables.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/fixedHeader.bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/responsive.bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/dataTables.bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    
@endsection

@section('content')
    <div class="container">


        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('/AdminPanel')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            {{$pageTitle}}
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">

                    <h4 class="m-t-0 m-b-30 header-title"><b>{{$pageTitle}}</b>
                        {{--
                                                <span class="pull-left">
                                                    <a href="{{url('/AdminPanel/Categories/create')}}"
                                                       class="btn btn-info btn-rounded w-md waves-effect waves-light">Add Company</a>
                                                </span>
                        --}}
                    </h4>

                    <div class="table-responsive">
                        <table id="datatable-fixed-header" class="table table-striped  table-colored table-info">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $x = 1; ?>
                            @foreach($categories as $category)
                                <tr id="row_{{$x}}">
                                    <th>{{$x}}</th>
                                    <td>{{$category->name}}</td>
                                    <td>
                                        <a href="{{url('/AdminPanel/Categories/'.$category->id)}}"
                                           class="btn actions btn-info waves-effect waves-light"> <i
                                                    class="fa fa-eye m-r-5"></i> <span>Show</span> </a>
                                        <div class="clearfix m-b-5"></div>
                                        <a href="{{url('/AdminPanel/Categories/'.$category->id.'/edit')}}"
                                           class="btn actions btn-success waves-effect waves-light"> <i
                                                    class="fa fa-edit m-r-5"></i> <span>Edit</span> </a>
                                        <div class="clearfix m-b-5"></div>

                                        <a href=" {{url('AdminPanel/Categories/'.$category->id)}}"
                                           onclick="event.preventDefault();document.getElementById('delete-form').submit();"
                                           class="btn actions btn-danger waves-effect waves-light">
                                            <i class="fa fa-trash m-r-5"></i> <span>Delete</span> </a>
                                        <form id="delete-form" action="{{url('AdminPanel/Categories/'.$category->id) }}"
                                              method="POST" style="display: none;">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                                <?php $x++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- container -->

@endsection

@section('script')
    <script src="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/jquery.dataTables.min.js"></script>
    <script src="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/dataTables.bootstrap.js"></script>
    <script src="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/dataTables.fixedHeader.min.js"></script>
    <script src="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/dataTables.responsive.min.js"></script>
    <script src="{{url('/')}}/AdminAssets/plugins/custom-datatables{{trans('Admin.folderDir')}}/responsive.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-responsive').DataTable();
            var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
        });
    </script>

    {{--<script>
        $('#delete-form').on('submit', function(){
            return confirm('Do you want to delete this item?');
        });
    </script>--}}
@endsection
