@extends('AdminPanel.Layouts.AuthMaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('/AdminPanel')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            {{$pageTitle}}
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">

            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-10">{{$pageTitle}}</h4>
                    <p class="text-muted small m-b-30">You can only view the category details</p>
                    <div class="row">
                        <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Category Name : {{$category->name}}</p>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container -->

@endsection
