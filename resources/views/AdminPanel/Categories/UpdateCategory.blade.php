@extends('AdminPanel.Layouts.AuthMaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('/AdminPanel')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            {{$pageTitle}}
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">

            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-10">{{$pageTitle}}</h4>
                    <p class="text-muted small m-b-30">You can only view the category details</p>
                    <div class="row">
                        <div class="col-md-12">
                            <form class="col-md-12" action="{{url('/AdminPanel/Categories/'.$category->id)}}"
                                  method="post">
                                @csrf
                                @method('PUT')

                                <div class="col-md-6">
                                    <div class="form-group @if($errors->has('category_name')) has-error @endif">
                                        <label class="control-label" for="category_name">Category Name</label>
                                        <input type="text" id="category_name" name="category_name"
                                               value="{{old('category_name') ? old('category_name') : $category->name}}"
                                               class="form-control"
                                               placeholder="ex : Category 1" >
                                        @if ($errors->has('category_name'))
                                            <label id="category_name-error" class="text-danger m-t-5"
                                                   for="category_name">{{ $errors->first('category_name') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="form-group m-b-0">
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btn btn-info waves-effect waves-light">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container -->

@endsection
