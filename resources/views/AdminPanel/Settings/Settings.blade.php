@extends('AdminPanel.Layouts.AuthMaster')

@section('content')
    <div class="container">


        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('/AdminPanel')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            {{$pageTitle}}
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-10">{{$pageTitle}}</h4>
                    <p class="text-muted small m-b-30">You can control site settings from this page</p>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{url('/AdminPanel/Settings')}}" method="post">
                                @csrf
                                <div class="col-md-6 m-b-5">
                                    <div class="form-group  @if($errors->has('site_name')) has-error @endif">
                                        <label class="control-label" for="site_name">Site Name</label>
                                        <input type="text" id="site_name" name="site_name"
                                               value="{{old('site_name') ? old('site_name') : $setting['site_name']->value}}"
                                               class="form-control" placeholder="ex : Task 1">
                                        @if ($errors->has('site_name'))
                                            <label id="site_name-error" class="text-danger m-t-5"
                                                   for="site_name">{{ $errors->first('site_name') }}</label>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6  m-b-5">
                                    <div class="form-group  @if($errors->has('site_email')) has-error @endif">
                                        <label class="control-label" for="site_email">Site Email Address</label>
                                        <input type="email" id="site_email" name="site_email"
                                               value="{{old('site_email') ? old('site_email') : $setting['site_email']->value}}"
                                               class="form-control" placeholder=" ex : task1@mail.com">
                                        @if ($errors->has('site_email'))
                                            <label id="site_email-error" class="text-danger m-t-5"
                                                   for="site_email">{{ $errors->first('site_email') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6  m-b-5">
                                    <div class="form-group @if($errors->has('site_keywords')) has-error @endif">
                                        <label class="control-label" for="site_keywords">Site Keywords</label>
                                        <textarea name="site_keywords" id="site_keywords" class="form-control" cols="30">
                                               {{old('site_keywords') ? old('site_keywords') : $setting['site_keywords']->value}}
                                        </textarea>
                                        @if ($errors->has('site_keywords'))
                                            <label id="site_keywords-error" class="text-danger m-t-5"
                                                   for="site_keywords">{{ $errors->first('site_keywords') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6  m-b-5">
                                    <div class="form-group @if($errors->has('site_description')) has-error @endif">
                                        <label class="control-label" for="site_description">Site Description</label>
                                        <textarea name="site_description" id="site_description" class="form-control"
                                                  cols="30">
                                               {{old('site_description') ? old('site_description') : $setting['site_description']->value}}
                                        </textarea>
                                        @if ($errors->has('site_description'))
                                            <label id="site_description-error" class="text-danger m-t-5"
                                                   for="site_description">{{ $errors->first('site_description') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2  m-b-5">
                                    <div class="form-group @if($errors->has('maintenance_mode')) has-error @endif">
                                        <label class="control-label" for="maintenance_mode">Maintenance Mode</label>
                                        <select name="maintenance_mode" id="maintenance_mode" class="form-control">
                                            <option value="enabled" {{old('maintenance_mode') ?  ( old('maintenance_mode') == 'enabled' ? 'selected' : '' )  : ($setting['maintenance_mode']->value  == 'enabled' ? 'selected' : '')}}>
                                                Enabled
                                            </option>
                                            <option value="disabled" {{old('maintenance_mode') ?  ( old('maintenance_mode') == 'disabled' ? 'selected' : '' )  : ($setting['maintenance_mode']->value  == 'disabled' ? 'selected' : '')}}>
                                                Disabled
                                            </option>
                                        </select>
                                        @if ($errors->has('maintenance_mode'))
                                            <label id="maintenance_mode-error" class="text-danger m-t-5"
                                                   for="maintenance_mode">{{ $errors->first('maintenance_mode') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-10  m-b-5">
                                    <div class="form-group @if($errors->has('maintenance_message')) has-error @endif">
                                        <label class="control-label" for="maintenance_message">Maintenance
                                            Message</label>
                                        <input type="text" id="maintenance_message" name="maintenance_message"
                                               value="{{old('maintenance_message') ? old('maintenance_message') : $setting['maintenance_message']->value}}"
                                               class="form-control"
                                               placeholder="ex : Sorry website is closed for maintenance try again later">
                                        @if ($errors->has('maintenance_message'))
                                            <label id="maintenance_message-error" class="text-danger m-t-5"
                                                   for="maintenance_message">{{ $errors->first('maintenance_message') }}</label>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-12 pull-left">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->

@endsection

@section('script')
    <script src="{{url('/')}}/ProjectAttachments/AdminAssets/assets/js/detect.js"></script>
    <script src="{{url('/')}}/ProjectAttachments/AdminAssets/assets/js/fastclick.js"></script>
    <script src="{{url('/')}}/ProjectAttachments/AdminAssets/assets/js/jquery.blockUI.js"></script>

@endsection
