<!DOCTYPE html>
<html lang="{{__('Admin.lang')}}" dir="{{__('Admin.dir')}}">
@include('AdminPanel.Layouts.Partials.Head')

<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="spinner-wrapper">
                <div class="rotator">
                    <div class="inner-spin"></div>
                    <div class="inner-spin"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- HOME -->
@yield('content')
<!-- END HOME -->

@include('AdminPanel.Layouts.Partials.FooterScripts')

</body>
</html>
