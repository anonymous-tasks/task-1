<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{url('/')}}/AdminAssets/assets/images/favicon.ico">
    <!-- App title -->
    <title>Zircos - Responsive Admin Dashboard Template</title>

    <!-- App css -->
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/core.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/components.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/icons.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/pages.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/menu.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/responsive.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/AdminAssets/assets/custom-css{{__('Admin.folderDir')}}/custom.css"
          rel="stylesheet" type="text/css"/>
    <!-- Sweet Alert -->
    <link href="{{url('/')}}/AdminAssets/plugins/custom-bootstrap-sweetalert{{__('Admin.folderDir')}}/sweet-alert.css"
          rel="stylesheet" type="text/css">

{{--
    <!-- undefined -->
    <link rel="stylesheet" href="{{url('/')}}/AdminAssets/plugins/switchery/switchery.min.css">
--}}

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- undefined -->
    <script src="{{url('/')}}/AdminAssets/assets/js/modernizr.min.js"></script>

    @yield('style')
</head>
