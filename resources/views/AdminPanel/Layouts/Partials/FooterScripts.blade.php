
<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.min.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/detect.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/fastclick.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.blockUI.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/waves.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.slimscroll.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.scrollTo.min.js"></script>
<script src="{{url('/')}}/AdminAssets/plugins/switchery/switchery.min.js"></script>

{{--
<!-- undefined -->
<!-- Counter js  -->
<script src="{{url('/')}}/AdminAssets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="{{url('/')}}/AdminAssets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- Dashboard init -->
<script src="{{url('/')}}/AdminAssets/assets/pages/jquery.dashboard.js"></script>
--}}

<!-- App js -->
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.core.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/js/jquery.app.js"></script>

<!-- Sweet-Alert  -->
<script src="{{url('/')}}/AdminAssets/plugins/custom-bootstrap-sweetalert{{__('Admin.folderDir')}}/sweet-alert.min.js"></script>
<script src="{{url('/')}}/AdminAssets/assets/pages/custom-jquery.sweet-alert.init{{__('Admin.folderDir')}}.js"></script>

@yield('script')
