<ul>
    <li class="menu-title">Browsing & Settings</li>
    <li>
        <a href="{{url('/AdminPanel')}}" class="waves-effect  {{$active=='Index' ? 'active' :''}}">
            <i class="mdi mdi-home"></i>
            <span> Dashboard </span> </a>
    </li>
    <li>
        <a href="{{url('/AdminPanel/Settings')}}" class="waves-effect  {{$active=='Settings' ? 'active' :''}}">
            <i class="mdi mdi-settings"></i>
            <span> Settings </span> </a>
    </li>

    <li class="menu-title">Control CMS</li>

    <li class="has_sub">
        <a href="javascript:void(0);"
           class="waves-effect  {{in_array($active,['Categories','CreateCategory']) ? 'active' :''}}">
            <i class=" mdi mdi-leaf"></i>
            <span>Categories</span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li  class=" {{$active=='Categories' ? 'active' :''}}"><a href="{{url('/AdminPanel/Categories')}}">All Categories</a></li>
            <li class=" {{$active=='CreateCategory' ? 'active' :''}}"><a href="{{url('/AdminPanel/Categories/create')}}">Add New Category</a></li>
        </ul>
    </li>

    <li class="has_sub">
        <a href="javascript:void(0);"
           class="waves-effect  {{in_array($active,['News','CreateNews']) ? 'active' :''}}">
            <i class="mdi mdi-grease-pencil"></i>
            <span>News</span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li  class=" {{$active=='News' ? 'active' :''}}"><a href="{{url('/AdminPanel/News')}}">All News</a></li>
            <li class=" {{$active=='CreateNews' ? 'active' :''}}"><a href="{{url('/AdminPanel/News/create')}}">Add News</a></li>
        </ul>
    </li>

    <li class="menu-title">Authenticated User</li>
    <li>
        <a  href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"
            class="waves-effect">
            <i class="mdi mdi-logout"></i>
            <span>Logout</span>
        </a>
    </li>

</ul>
