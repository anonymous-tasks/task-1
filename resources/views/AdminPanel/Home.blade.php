@extends('AdminPanel.Layouts.AuthMaster')

@section('content')
    <div class="container">

        <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 m-b-15">
                <h3> Welcome To Admin Panel</h3>
            </div>

            <div class="col-lg-4 col-md-8 col-sm-12">
                <div class="card-box widget-box-one">
                    <div class="wigdet-one-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Categories Count</p>
                        <h2 class="text-danger"><span data-plugin="counterup">{{\App\Category::count()}}</span></h2>
                    </div>
                </div>
            </div><!-- end col -->

            <div class="col-lg-4 col-md-8 col-sm-12">
                <div class="card-box widget-box-one">
                    <div class="wigdet-one-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow">News Count</p>
                        <h2 class="text-info"><span data-plugin="counterup">{{\App\News::count()}}</span></h2>
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->

    </div> <!-- container -->

@stop
