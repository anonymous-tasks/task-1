<?php

return [
    'lang'=>'en',
    'langReverse'=>'ar',
    'filesDir'=>'.ltr',
    'filesDirReverse'=>'.rtl',
    'dir'=>'ltr',
    'folderDir'=>'',
    'dirReverse'=>'rtl',
    'dirName'=>'left',
    'dirReverseName'=>'right',

    'email'=>'Email',
    'password'=>'Password',
    'remember'=>'Remember Me',
    'login'=>'Login',
    'forgetPassword'=>'Forgot your password?',
];
