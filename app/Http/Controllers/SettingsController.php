<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function getUpdateSettings()
    {
        $setting = Setting::get()->keyBy('key');
        return view('AdminPanel.Settings.Settings')
            ->with([
                'pageTitle' => 'Site Settings',
                'active' => 'Settings',
                'setting' => $setting
            ]);
    }

    public function postUpdateSettings(Request $request)
    {
        $this->validate($request, [
            'site_name' => 'required|string|max:190',
            'site_email' => 'required|email|max:190',
            'site_keywords' => 'nullable',
            'site_description' => 'nullable',
            'maintenance_mode' => 'required|in:enabled,disabled',
            'maintenance_message' => 'required_if:maintenance_mode,disabled|max:180',
        ]);
        foreach ($_POST as $key => $value) {
            if (Setting::where('key', $key)->exists()) {
                if ($key != '_token') {
                    $edit = Setting::where('key', $key)->first();
                    $edit->value = $value;
                    $edit->save();
                }
            } else {
                if ($key != '_token') {
                    $save = New Setting();
                    $save->key = $key;
                    $save->value = $value;
                    $save->LinkedID = '';
                    $save->save();
                }
            }
        }
        session()->flash('Success', 'Data Updated Successfully!');
        return back();

    }


}
