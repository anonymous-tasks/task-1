<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        return view('AdminPanel.Categories.AllCategories')
            ->with([
                'pageTitle'=>'All Categories',
                'active'=>'Categories',
                'categories'=>$categories,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('AdminPanel.Categories.CreateCategory')
            ->with([
                'pageTitle'=>'Add New Category',
                'active'=>'Categories',
            ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required|max:180',
        ]);
        $category = new Category();
        $category->name = $request['category_name'];
        if ($category->save()) {
            session()->flash('Success', 'Category Added Successfully !');
            return back();
        } else {
            session()->flash('Faild', 'Sorry , unknown error occured ! please try again later ');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::findOrFail($id);
        return view('AdminPanel.Categories.SingleCategory')
            ->with([
                'pageTitle'=>$category->name,
                'active'=>'Categories',
                'category'=>$category,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::findOrFail($id);
        return view('AdminPanel.Categories.UpdateCategory')
            ->with([
                'pageTitle'=>$category->name,
                'active'=>'Categories',
                'category'=>$category,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required|max:180',
        ]);
        $category = Category::find($id);
        $category->name = $request['category_name'];
        if ($category->update()) {
            session()->flash('Success', 'Category Updated Successfully !');
            return redirect('/AdminPanel/Categories/');
        } else {
            session()->flash('Faild', 'Sorry , unknown error occured ! please try again later ');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        if ($category->delete()) {
            session()->flash('Success', 'Category Deleted Successfully !');
            return redirect('/AdminPanel/Categories');
        } else {
            session()->flash('Faild', 'Sorry , unknown error occured ! please try again later ');
            return back();
        }
    }
}
