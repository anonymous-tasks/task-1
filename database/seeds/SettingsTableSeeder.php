<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key' => 'site_name',
                'value' => 'Task 1',
            ],
            [
                'key' => 'site_email',
                'value' => '',
            ],
            [
                'key' => 'site_keywords',
                'value' => '',
            ],
            [
                'key' => 'site_description',
                'value' => '',
            ],
            [
                'key' => 'maintenance_mode',
                'value' => '',
            ],
            [
                'key' => 'maintenance_message',
                'value' => '',
            ],
    ]);
    }
}
